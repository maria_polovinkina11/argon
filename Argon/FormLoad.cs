﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Argon
{
    public partial class FormLoad : Form
    {

        public delegate void MessageHandler();
        // Выполняемые действия при загрузке.
        private MessageHandler message;
        // Завершение загрузки.
        private bool loadCompl = false;

        public FormLoad()
        {
            InitializeComponent();
        }

        private void FormLoad_Load(object sender, EventArgs e)
        {
            timer_load.Start();
        }

        /// <summary>
        /// Создать форму загрузки.
        /// </summary>
        /// <param name="text">Текст заголовка формы.</param>
        /// <param name="mes">Делегат выполняемых действий.</param>
        public FormLoad(string text, MessageHandler mes)
        {
            InitializeComponent();

            this.Text = text;
            message = mes;
        }

        private void FormLoad_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!loadCompl) e.Cancel = true;
        }

        private void timer_load_Tick(object sender, EventArgs e)
        {
            timer_load.Stop();

            message();
            loadCompl = true;
            Close();
        }
    }
}

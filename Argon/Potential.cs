﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argon
{
    /// <summary>
    /// Потенциал взаимодействия.
    /// </summary>
    public abstract class Potential
    {
        public abstract double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false);
    }


    public class PotentialLennardJons : Potential
    {
        /// <summary>
        /// Параметры потенциала Леннарда-Джонса.
        /// </summary>
        private struct ParamPotential
        {
            public double e, a, ro, r1, r2;
            public ParamPotential(double e, double a, double ro, double r1, double r2)
            {
                this.e = e;//эпсиланд              
                this.a = a;//сигма
                this.ro = ro;//равновесное расстояние между цнтрами атомов
                this.r1 = r1;// 
                this.r2 = r2;// 
            }


        }

        /// <summary>
        /// Параметры для различных соединений атомов.
        /// </summary>

        private ParamPotential paramOfAr;
        public PotentialLennardJons(double latParAr, double latStruct)
        {
            double ar = 0.0103 * 1.66054e-27;
            double sq = Math.Pow(2, 1.0 / 6.0);
            paramOfAr = new ParamPotential(ar, 0.382 / sq, 0.382, (1.15) * 0.382, (1.75) * 0.382);
        }

        /// <summary>
        /// Функция обрезания K(r).
        /// </summary>
        /// <param name="potential">Параметры потенциала.</param>
        /// <param name="radius">Расстояние до атома-соседа.</param>
        /// <returns></returns>
        private double PE_Function(ParamPotential potential, double radius)
        {
            double U = Math.Pow(1 - Math.Pow((radius - potential.r1) / (radius - potential.r2), 2), 2);
            if (radius <= potential.r1) return 1.0;
            else if (radius >= potential.r1 && radius <= potential.r2) return 1.0 * U;
            else return 0.0;

        }

        private double U_Function(ParamPotential potential, double radius)
        {
            return 1.0 * potential.e * (Math.Pow(potential.a / radius, 12) - (Math.Pow(potential.a / radius, 6)));
        }



        /// <summary>
        /// Потенциальная энергия выбранного атома.
        /// </summary>
        /// <param name="selAtom">Выбранный атом.</param>
        /// <param name="lengthSystem">Размер кубической расчётной ячейки.</param>
        /// <param name="force">Расчёт энергии взаимодействия выбранного атома.</param>
        /// <returns></returns>
        public override double PotentialEnergy(Atom selAtom, double lengthSystem, bool force = false)
        {
            double atomEnergy = 0.0;


            for (int j = 0; j < selAtom.Neighbours.Length; j++)
            {
                double R = Vector.MagnitudePeriod(selAtom.Coordinate, selAtom.Neighbours[j].Coordinate, lengthSystem);

                ParamPotential potentialIJ = paramOfAr;
                double U = U_Function(potentialIJ, R) * PE_Function(potentialIJ, R);
                atomEnergy += U;
            }
            if (!force) atomEnergy /= 2.0;

            return atomEnergy;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Argon
{
    public partial class Structure
    {

       

        /// <summary>
        /// Потенциал Стиллинджера-Вебера.
        /// </summary>
        private PotentialLennardJons pLennard;

        /// <summary>
        /// Выбранный потенциал.
        /// </summary>
        public Potential SelPotential
        {
            private set { selPotential = value; }
            get { return selPotential; }
        }
        private Potential selPotential;

        /// <summary>
        /// Кубическая кристаллическая решётка.
        /// </summary>
        public readonly Atom[] Struct;

        /// <summary>
        /// Количество атомов в элементарной атомной ячейке.
        /// </summary>
        public const int NumberAtoms = 8;

        /// <summary>
        /// Количество атомов в структуре.
        /// </summary>
        public readonly int StructNumAtoms;

        /// <summary>
        /// Размер структуры.
        /// </summary>
        public readonly int StructLength;

        /// <summary>
        /// Параметр решётки структуры.
        /// </summary>
        public readonly double StructLatPar;

        /// <summary>
        /// Создание кубической структуры.
        /// </summary>
        /// <param name="length">Размер структуры.</param>
        public Structure(int length, bool Ar, int numatom, List<int> idx,  bool vacancy)
        {
            // Задание параметров.
            this.StructLength = length;
            this.StructNumAtoms = TotalNumAtoms(this.StructLength);
            this.StructLatPar = AtomLatticeParam(Ar);
            this.Struct = new Atom[this.StructNumAtoms];

            this.pLennard = new PotentialLennardJons(AtomParams.LATPAR_AR, this.StructLatPar);
            this.SetPotential();

            AtomType setType = AtomType.Ar;



            double step = this.StructLatPar / this.StructLength;
            int atomIdx = -1;
            // Размещение атомов.

            for (double x = 0; x < this.StructLatPar-step; x+=step)
            {
                for (double y = 0; y < this.StructLatPar-step; y+=step)
                {
                   // double koeff = 0.25d * this.StructLatPar;

                    Vector posCell = new Vector(x, y);

                    this.Struct[++atomIdx] = new Atom(posCell, setType, atomIdx);
                    /*        if((this.StructLength%2)==0)
                            {
                                double koeff = 0.25d * this.StructLatPar;

                                Vector posCell = new Vector(x, y) * this.StructLatPar;

                                this.Struct[++atomIdx] = new Atom(posCell, setType, atomIdx);
                                this.Struct[++atomIdx] = new Atom(new Vector(0, 4) * koeff + posCell, setType, atomIdx);
                                this.Struct[++atomIdx] = new Atom(new Vector(4, 0) * koeff + posCell, setType, atomIdx);
                                this.Struct[++atomIdx] = new Atom(new Vector(4, 4) * koeff + posCell, setType, atomIdx);
                            }*/


                    //if (atomIdx == 63) break;
                }

            }
                      /*  for (int x = 0; x < this.StructLength; ++x)
                        {
                            for (int y = 0; y < this.StructLength; ++y)
                            {
                             //   for (int z = 0; z < this.StructLength; ++z)

                                    double koeff = 0.25d * this.StructLatPar;

                                    Vector posCell = new Vector(x, y) * this.StructLatPar;

                                    this.Struct[++atomIdx] = new Atom(posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(0, 1) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(0, 2) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(0, 3) * koeff + posCell, setType, atomIdx);

                                    this.Struct[++atomIdx] = new Atom(new Vector(1, 0) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(1, 1) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(1, 2) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(1, 3) * koeff + posCell, setType, atomIdx);

                                    this.Struct[++atomIdx] = new Atom(new Vector(2, 0) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(2, 1) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(2, 2) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(2, 3) * koeff + posCell, setType, atomIdx);

                                    this.Struct[++atomIdx] = new Atom(new Vector(3, 0) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(3, 1) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(3, 2) * koeff + posCell, setType, atomIdx);
                                    this.Struct[++atomIdx] = new Atom(new Vector(3, 3) * koeff + posCell, setType, atomIdx);



                            }*/



                    if (vacancy == true)
            {
                for (int i = 0; i < this.StructNumAtoms; i++)
                {
                    for (int j = 0; j < idx.Count; ++j)
                    {
                       if(i== idx[j])
                        Struct[i].Vacancy = true;
                    }
                }
                this.SearchNeighbours(/*true, idx*/);
            }
            else
            this.SearchNeighbours(/*false, idx*/);
        }

        /// <summary>
        /// Количество атомов в структуре.
        /// </summary>
        /// <param name="length">Размер структуры.</param>
        /// <returns></returns>
        public static int TotalNumAtoms(int length)
        {
            return length * length;
        }

        /// <summary>
        /// Потенциал взаимодействия.
        /// </summary>
        public void SetPotential()
        {
            this.SelPotential = pLennard;
        }

        /// <summary>
        /// Параметр решётки структуры.
        /// </summary>
        /// <returns></returns>
        public static double AtomLatticeParam(bool Ar)
        {
            double Param = 0;
            Param = AtomParams.LATPAR_AR;


            return Param;
        }

        /// <summary>
        /// Поиск соседей у всех атомов структуры.
        /// </summary>
        public void SearchNeighbours(/*bool vacancy,  List<int> idx*/)
        {
            double L = this.StructLength * this.StructLatPar;
            double radiusOfNeighbours = new Vector(this.StructLatPar * 0.25d, this.StructLatPar * 0.25d).Magnitude();
            radiusOfNeighbours += radiusOfNeighbours * 0.2d;
           /* if (vacancy == false)
            {*/
                for (int i = 0; i < this.StructNumAtoms; i++)
                {

                    this.Struct[i].SearchFirstNeighbours(this.Struct, L, radiusOfNeighbours);
                }

                for (int j = 0; j < this.StructNumAtoms; j++)
                {
                    this.Struct[j].SearchSecondNeighbours();
                    this.Struct[j].SumNeighbours();
                }
          /*  }
            else
            {
                for (int i = 0; i < this.StructNumAtoms; i++)
                {
                    for (int k = 0; k < idx.Count; k++)
                    {
                        if (i == idx[k])
                        {
                            i++;
                        }

                        this.Struct[i].SearchFirstNeighbours(this.Struct, L, radiusOfNeighbours);
                    }
                }

                for (int j = 0; j < this.StructNumAtoms; j++)
                {
                    for (int k = 0; k < idx.Count; k++)
                    {
                        if (j == idx[k])
                        {
                            j++;
                        }
                        this.Struct[j].SearchSecondNeighbours();
                        this.Struct[j].SumNeighbours();
                    }
                }
            }*/
        }

        /// <summary>
        /// Подсчёт полной потенциальной энергии системы.
        /// </summary>
        /// <returns></returns>
        public double PotentialEnergy()
        {
            double sumEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = 0; i < this.StructNumAtoms; i++)
            {
                this.Struct[i].PotentialEnergy(this.SelPotential, L, false);

                sumEnergy += this.Struct[i].PotEnergy;
            }

            return sumEnergy;
        }

        /// <summary>
        /// Подсчёт потенциальной и кинетической энергии.
        /// </summary>
        /// <param name="potEnergy">Потенциальная энергия.</param>
        /// <param name="kinEnergy">Кинетическая энергия.</param>
        public void Energy(out double potEnergy, out double kinEnergy)
        {
            potEnergy = 0.0d;
            kinEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = 0; i < this.StructNumAtoms; i++)// Перебор атомов в ячейке
            {
                this.Struct[i].PotentialEnergy(this.SelPotential, L, false);

                potEnergy += this.Struct[i].PotEnergy;
                kinEnergy += this.Struct[i].KineticEnergy();
            }
        }

        /// <summary>
        /// Энергия равновесного состояния (эВ).
        /// </summary>
        /// <param name="temperature">Температура (в Кельвинах).</param>
        /// <param name="numAtoms">Количество атомов структуры.</param>
        /// <returns></returns>
        public static double TemperatureEnergy(double temperature, int numAtoms)
        {
            return (3.0d / 2.0d) * AtomParams.BOLTZMANN * numAtoms * temperature;
        }

        /// <summary>
        /// Подсчёт потенциальной и кинетической энергии части структуры.
        /// </summary>
        /// <param name="potEnergy">Потенциальная энергия.</param>
        /// <param name="kinEnergy">Кинетическая энергия.</param>
        /// <param name="startIdx">Начальный индекс атома.</param>
        /// <param name="endIdx">Конечный индекс атома.</param>
        public void Energy(out double potEnergy, out double kinEnergy, int startIdx, int endIdx)
        {
            potEnergy = 0.0d;
            kinEnergy = 0.0d;
            double L = this.StructLength * this.StructLatPar;

            for (int i = startIdx; i < endIdx; i++)
            {
                if (this.Struct[i].Vacancy == true)
                {
                    i++;
                }
                this.Struct[i].PotentialEnergy(this.SelPotential, L, false);

                potEnergy += this.Struct[i].PotEnergy;
                kinEnergy += this.Struct[i].KineticEnergy();
            }
        }

        /// <summary>
        /// Суммирование подсчитанной потенциальной энергии атомов структуры.
        /// </summary>
        /// <returns></returns>
        public double SumPotentialEnergy()
        {
            double potEnergy = 0.0d;

            for (int i = 0; i < this.StructNumAtoms; i++)
                potEnergy += this.Struct[i].PotEnergy;

            return potEnergy;
        }

        public double FindEnergyVacancy(double E_ideal, double E)
        {
            double n = this.StructNumAtoms;
            double N = (double)((n - 1) / n);
            return E_ideal * N - E;
        }


        /// <summary>
        /// Впомогательный метод для подсчёта атомов для ПКФ в определённой сфере
        /// </summary>
        /// <param name="typeAr">Полученное значение количества атомов аргона</param>
        /// <param name="r">Радиус просмотра</param>
        /// <param name="rdr">Толщина сферы</param>
        /// <param name="startIdx">Начальный индекс атома в массиве перебираемых атомов</param>
        /// <param name="endIdx">Конечный индекс атома в массиве перебираемых атомов</param>
        private void CalcAtoms(out int typeAr, double r, double rdr, int startIdx, int endIdx)
        {
            typeAr = 0;
            double L = this.StructLength * this.StructLatPar;

            for (int i = startIdx; i < endIdx; i++)
            {
                for (int j = 0; j < StructNumAtoms; j++)
                {
                    if (i == j) continue;

                    double radius = Vector.MagnitudePeriod(Struct[i].Coordinate, Struct[j].Coordinate, L);
                    if (radius > r && radius <= rdr)
                    {
                        typeAr++;

                    }
                }
            }
        }
        /// <summary>
        /// Парная корреляционная функция (ПКФ)
        /// </summary>
        /// <param name="masSiGe">Выходной массив значений функции g(r)</param>
        /// <returns></returns>
        public double[] PairCorrelationFunction(out double[] masAr)
        {
            int length = 100;
            double rmax = StructLatPar;
            double dr = rmax / length;
            double l = dr;

            double[] ra = new double[length];
            masAr = new double[length];

            for (int s = 0; s < length; s++)
            {
                double r = dr * s;
                int typeAr = 0;
          


                int half = StructNumAtoms / 8;
                int[] halfSize = new int[9] { 0, half, half * 2, half * 3, half * 4, half * 5, half * 6, half * 7, StructNumAtoms };
                int[] masTypeAr = new int[8];

                Task[] tasks = new Task[8]
                {
                    new Task(delegate { CalcAtoms(out masTypeAr[0], r, r+l, halfSize[0], halfSize[1]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[1], r, r+l, halfSize[1], halfSize[2]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[2], r, r+l, halfSize[2], halfSize[3]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[3], r, r+l, halfSize[3], halfSize[4]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[4], r, r+l, halfSize[4], halfSize[5]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[5], r, r+l, halfSize[5], halfSize[6]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[6], r, r+l, halfSize[6], halfSize[7]); }, TaskCreationOptions.AttachedToParent),
                    new Task(delegate { CalcAtoms(out masTypeAr[7], r, r+l, halfSize[7], halfSize[8]); }, TaskCreationOptions.AttachedToParent)
                };

                for (int task = 0; task < tasks.Length; task++)
                    tasks[task].Start();
                Task.WaitAll(tasks);

                for (int i = 0; i < masTypeAr.Length; i++)
                {
                    typeAr += masTypeAr[i];
                }

                for (int t = 0; t < tasks.Length; t++)
                    tasks[t].Dispose();

              

                ra[s] = Math.Round(r / StructLatPar, 2);
                masAr[s] = typeAr / (double)StructNumAtoms;
              

                double add = 0;
                if (r != 0 && l != 0) add = (Math.Pow(StructLatPar * StructLength, 3) / StructNumAtoms) / (4 * Math.PI * l * r * r);
                masAr[ s] *= add;
           
            }

            return ra;
        }


    }
}

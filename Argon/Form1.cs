﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibDrawingGraphs;

namespace Argon
{
    public partial class Form1 : Form
    {

        Structure str;
        //Массивы энергий
        double[] kinEnergy, potEnergy, oPotEnergy, sumEnergy, masTimeEnergy;
       
        private FormOptions.ModelingParams modelParams;
        Random RandShift, RandNumGen, Rand;
        Bitmap bmp;
        Graphics graph;
        SolidBrush AroBrush = new SolidBrush(Color.LightSkyBlue);
        Pen ArgoPen = new Pen(Color.Purple, 1);

        SolidBrush AroBrushV = new SolidBrush(Color.LightBlue);
        Pen ArgoPenV = new Pen(Color.LightBlue, 1);
        Point center;
     //   bool Vacan;
        List<Point> AtomCenter; // = new List<Point>();
        List<Point> AroStep;

        int timer_index;


        void Init()       // Функция инцилизации компонентов
        {

        //    Vacan = Convert.ToBoolean(checkBox1.Checked);
        }
        private void btn_calculateEnergy_Click(object sender, EventArgs e)
        {

            double energy = str.PotentialEnergy() * 1E24;
            txt_sumEnergy.Text = energy.ToString("f6");
            txt_atomEnergy.Text = (energy / str.StructNumAtoms).ToString("f6");
        }

        private void button_openToCoord_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenCoordAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        
        private void buttonToNeighborOne_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenFirstNeighboursdAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonbuttonToNeighborTwo_Click(object sender, EventArgs e)
        {
            if (!OutputInfo.OpenSecondNeighboursdAtomsFile()) MessageBox.Show("Данного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

     
        public Form1()
        {
            InitializeComponent();
           // Size size = pictureBox1.Size;
           // center = new Point(pictureBox1.Width / 2, pictureBox1.Height / 2);

           // bmp = new Bitmap(size.Width, size.Height);
           // graph = Graphics.FromImage(bmp);
        }

        //Метод подсчёта количества атомов структуры в зависимости от её размера
        private void numUD_sizeCells_ValueChanged(object sender, EventArgs e)
        {
            int sizeStruct = (int)numUD_n.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);
            txt_numAtoms.Text = numAtoms.ToString();
            double value = Math.Round(numAtoms * 3.0 / 100) * 100;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Init();
            //graph.Clear(Color.LightBlue);
            //AtomCenter = new List<Point>();
            pictureBox1.Image = bmp;
            // FirstT.Clear();

            int a = 17; // равновесное растояние
            int numAtoms = Structure.TotalNumAtoms((int)numUD_n.Value);
            double c, r1, r2;
            AroStep = new List<Point>();
            //AtomCenter.CopyTo(AroStep);

            Random ran = new Random();

            c = (0.1 / 0.25) * a;

            for (int i = 0; i < AtomCenter.Count; i++)
            {
                r1 = ran.NextDouble();
                r2 = ran.NextDouble();
                Point P = new Point();
                P.X = (int)(AtomCenter[i].X + (c * (-1 + 2 * r1)));
                P.Y = (int)(AtomCenter[i].Y + (c * (-1 + 2 * r2)));

                AroStep.Add(P);
            }

            for (int i = 0; i < numAtoms; i++)
            {
                r1 = ran.NextDouble();
                r2 = ran.NextDouble();
                double x, y;
                x = str.Struct[i].Coordinate.x + (c * (-1 + 2 * r1));
                y = str.Struct[i].Coordinate.y + (c * (-1 + 2 * r2));

                //str.Struct[i].Coordinate.x = x;
                //str.Struct[i].Coordinate.y = y;

               
            };
            timer1.Start();

        }

     

        public void DrawAro(Graphics graph, Pen spen, ref List<Point> Aro) // рисование равновестного состояния и вчисления центров
        {
            Size size = pictureBox1.Size;
            Aro.Clear();

            int numAtoms = Structure.TotalNumAtoms((int)numUD_n.Value);
            int Numer = (int)Math.Sqrt(numAtoms);
            int[] AroX = new int[Numer+1];
            int[] AroY = new int[Numer+1];

            //int Xmax = size.Width;
            //int Ymax = size.Height;
            for (int i = 0; i < (int)(Numer/2); i++)
            {
                AroX[i] = center.X + i*20;
            }
            for (int i = 4; i < Numer; i++)
            {
                AroX[i] = center.X - (i-3)*20;
            }
            for (int i = 0; i < (int)(Numer / 2); i++)
            {
                AroY[i] = center.Y + i*20;
            }
            for (int i = 4; i < Numer; i++)
            {
                AroY[i] = center.Y - (i-3)*20;
            }

            for (int u = 0; u < Numer; u++) // заполнение для центра
            {
                for (int O = 0; O < Numer; O++)
                {
                    Point a = new Point();
                    a.X = AroX[O];
                    a.Y = AroY[u];
                    Aro.Add(a);
                }
            }
            // graph.DrawEllipse(spen, (float)(center.X), (float)(center.Y), 20, 20);

            for (int u = 0; u < Aro.Count; u ++)
            {
                graph.DrawEllipse(spen, (float)(Aro[u].X), (float)(Aro[u].Y), 20, 20);
            }

        }

        private void txt_numAtoms_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            OutputInfo.Start();
          

            modelParams = new FormOptions.ModelingParams(0, 0, true);
        }

        // кнопка "Создать"
        private void btn_createStructure_Click(object sender, EventArgs e)
        {

            Init(); // РИСОВАНИЕ СОСТОЯНИЯ РАВНОВЕСИЯ
            //graph.Clear(Color.LightBlue);
            AtomCenter = new List<Point>();
          // DrawAro(graph, ArgoPen, ref AtomCenter);

           // pictureBox1.Image = bmp;

 


            int sizeStruct = (int)numUD_n.Value;//размер ячейки
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);//количество атомов в структуре

            double procent = Convert.ToDouble(textBox_procent.Text)/100.00;//процент удаляемых атомов

            int min = 0;//диапазон для генерации чисел
            int max = numAtoms-1;
            var s = (int)(numAtoms*procent);
            Rand = new Random();//элемент класса Random
            List<int> Index = new List<int>();

            int idx0 = Rand.Next(min, max);
            Index.Add(idx0);
            int iPhon13 = 0;

            for ( ; ; )
            {
                int idx =  Rand.Next(min, max);
                int con = Index.Count;
                bool Pof = true;

                for (int o = 0; o < con; o++)
                {
                    if (idx == Index[o]) Pof = false;
                }

                if (Pof) { Index.Add(idx); iPhon13++; }
                if (iPhon13 == (s - 1)) break;

            }

            if (checkBox_Vacancy.Checked) 
            {
                str = new Structure((int)numUD_n.Value, true, (int)numAtoms, Index, true);
            }
            else 
            str = new Structure((int)numUD_n.Value, true, (int)numAtoms, Index, false);

            pictureBox1.Width = (int)(str.Struct[numAtoms - 1].Coordinate.x * 50) * 4;
            pictureBox1.Height = (int)(str.Struct[numAtoms - 1].Coordinate.y * 50) * 4;
            Size size = pictureBox1.Size;


            center = new Point(pictureBox1.Width / 4, pictureBox1.Height / 4);

            bmp = new Bitmap(size.Width, size.Height);
            graph = Graphics.FromImage(bmp);
            graph.Clear(Color.LightBlue);

            for (int i = 0; i < numAtoms; i++)
            {
                if (str.Struct[i].Vacancy == true) //i++;
                {
                    graph.FillEllipse(AroBrushV, (float)(str.Struct[i].Coordinate.x * 100 + center.X), (float)(str.Struct[i].Coordinate.y * 100 + center.Y), 15, 15);
                    graph.DrawEllipse(ArgoPenV, (float)(str.Struct[i].Coordinate.x * 100 + center.X), (float)(str.Struct[i].Coordinate.y * 100 + center.Y), 15, 15);
                }
                else
                {
                    graph.FillEllipse(AroBrush, (float)(str.Struct[i].Coordinate.x * 100 + center.X), (float)(str.Struct[i].Coordinate.y * 100 + center.Y), 15, 15);
                    graph.DrawEllipse(ArgoPen, (float)(str.Struct[i].Coordinate.x * 100 + center.X), (float)(str.Struct[i].Coordinate.y * 100 + center.Y), 15, 15);
                }
            }
                pictureBox1.Image = bmp;
           // str.Finalize();
        }

        private void numUD_sizeCells_ValueChanged_1(object sender, EventArgs e)
        {
            int sizeStruct = (int)numUD_n.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);
            txt_numAtoms.Text = numAtoms.ToString();
       //     double value = Math.Round(numAtoms * 3.0 / 100) * 100;
        }

        private void button_StartRelax_Click(object sender, EventArgs e)
        {
            //Определяем параметры
            int numStep = (int)numUD_numStepRelax.Value;
            double temperature = (double)numUD_tmpr.Value;
            double shift = (double)numUD_shiftAtoms.Value;
            bool normalize = checkBox_startVelocity.Checked;
            int stepNorm = (int)numUD_stepNorm.Value;
            //  int stepDev = (int)numUD_step.Value;

          

            //Формируем шаги по времени
            List<int> stepDev = new List<int>(listBox_step.Items.Count);
            for (int i = 0; i < listBox_step.Items.Count; i++)
                stepDev.Add((int)listBox_step.Items[i]);



            /*    if (modelParams.seedShift != 0)
                    RandShift = new Random(modelParams.seedShift);
                else */
            RandShift = RandNumGen = new Random();

            Relaxation relax = new Relaxation(str);

            FormLoad.MessageHandler handler = delegate
            {
                relax.Initialization(RandNumGen, RandShift, temperature, shift, normalize,
                    stepDev.ToArray(), (double)numUD_multStepRelax.Value, numStep, stepNorm);

                relax.CalculatedTime();
            };

            FormLoad formLoad = new FormLoad("Подготовка данных", handler);
            formLoad.ShowDialog();

            if (MessageBox.Show(String.Format("Примерное время процесса - {0}h {1}m\nПродолжить?", relax.ProcTime.Hours, relax.ProcTime.Minutes),
                "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;

            //Проверяем и очищаем файлы Excel перед релаксацией
            bool success = OutputInfo.ClearEnergyFile();
            if (!success)
                if (MessageBox.Show("Обнаружено открытие используемого файла Excel - данные не будут записаны. Продолжить выполнение?",
                    "Внимание", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) return;



            // Релаксация

            //if (modelParams.minMainForm)
            //{
            //    this.WindowState = FormWindowState.Minimized; ЗАКРЫТИЕ СТАРОГО ОКНА
            //    this.ShowInTaskbar = false;
            //}

            //Проводим релаксацию
            Task task = Task.Factory.StartNew(delegate { relax.Iterations(); }, TaskCreationOptions.LongRunning);

            FormConsole formConsole = new FormConsole(relax);
            formConsole.ShowDialog(); //// ОТКРЫТИЕ ИАЛОГОВОГО ОКНА

            //if (modelParams.minMainForm)
            //{
            //    this.ShowInTaskbar = true; ЗАНОВА ОТКРЫВАЕМ СТАРОЕ ОКНО
            //    this.WindowState = FormWindowState.Normal;
            //}



            handler = delegate
            {
                btn_calculateEnergy_Click(null, null);


                //kinEnergy, potEnergy, oPotEnergy, sumEnergy, masTimeEnergy;
                kinEnergy = new double[relax.GetMasTimeStep.Length];
                potEnergy = new double[relax.GetMasTimeStep.Length];
                masTimeEnergy = new double[relax.GetMasTimeStep.Length];
                double[] time = new double[relax.GetMasTimeStep.Length];

                //Получаем энергию
                sumEnergy = new double[kinEnergy.Length];
                oPotEnergy = new double[kinEnergy.Length];
                for (int i = 0; i < kinEnergy.Length; i++)
                {
                    time[i] = relax.GetMasTimeStep[i];
                    kinEnergy[i] = relax.GetMasKinEnergy[i] * 1E12;
                    potEnergy[i] = relax.GetMasPotEnergy[i] * 1E12;
                    masTimeEnergy[i] = relax.GetMasTimeStep[i];

                    oPotEnergy[i] = (potEnergy[i] - potEnergy[0]) * 1E12;
                    sumEnergy[i] = (oPotEnergy[i] + kinEnergy[i]) * 1E12;
                  //  timer1.Start(); //////
                }
                ////////////////////////////////////////


            };
            formLoad = new FormLoad("Сохранение данных", handler);
            formLoad.ShowDialog();

            this.Activate();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            bool flag = ProvercaRavno(AtomCenter, AroStep);


         //   int a = 20; // равновесное растояние
         //   double c, r1, r2;

            //AtomCenter.CopyTo(AroStep);
            int sizeStruct = (int)numUD_n.Value;
            int numAtoms = Structure.TotalNumAtoms(sizeStruct);


            if (flag)
            {

                

                for (int i = 0; i < numAtoms; i++)
                {


                    graph.FillEllipse(AroBrush, (float)(str.Struct[i].Coordinate.x * 200 + center.X ), (float)(str.Struct[i].Coordinate.y * 200 + center.Y ), 17, 17);
                    graph.DrawEllipse(ArgoPen, (float)(str.Struct[i].Coordinate.x * 200 + center.X ), (float)(str.Struct[i].Coordinate.y * 200 + center.Y ), 17, 17);
                }
                //pictureBox1.Image = bmp;
                //for (int u = 0; u < AroStep.Count; u++)
                //{
                //    graph.DrawEllipse(ArgoPen, (float)(AroStep[u].X), (float)(AroStep[u].Y), 20, 20);
                   
                //}
               pictureBox1.Image = bmp;
                ////////////нужно сделать чтоб менялось до равновесного, как бы, после рисовашки считалось для следущего состояния
                //Random ran = new Random();
               
                //c = (0.1 / 0.25) * a;

                //List<Point> AroStep1 = new List<Point>();
                //for (int i = 0; i < AroStep.Count; i++)
                //{
                //    r1 = ran.NextDouble();
                //    r2 = ran.NextDouble();
                //    Point P = new Point();
                //    P.X = (int)(AroStep[i].X + (c * (-1 + 2 * r1)));
                //    P.Y = (int)(AroStep[i].Y + (c * (-1 + 2 * r2)));

                //    AroStep1.Add(P);
                //}
                //AroStep.Clear();

                //for (int i = 0; i < AroStep1.Count; i++)
                //{
                //    AroStep.Add(AroStep1[i]);
                //}
            }
            else
            {

                timer1.Stop();
                MessageBox.Show("Stop");

            }
           //pictureBox1.Image = bmp;
           if (timer_index > 50) timer1.Stop(); // ВРЕМЕННО ПОКА НЕТ РЕЛАКСАЦИИ ОТКЛЮЧАТЬ ТАЙМЕР


            timer_index++;
        }

        public bool ProvercaRavno(in List<Point> Cen, in List<Point> Step) // проверка на равновесие
        {
            bool flag = true;
            //  int numAtoms = Structure.TotalNumAtoms((int)numUD_sizeCells.Value);
            for (int i = 0; i < AroStep.Count; i++)
            {
                if (Math.Abs(Cen[i].X - Step[i].X) < 1 && Math.Abs(Cen[i].Y - Step[i].Y) < 1) flag = false;
               // if ( Math.Abs(Cen[i].Y - Step[i].Y) < 1) flag = false;
            }
            
            return flag;
        }

        
    }
}

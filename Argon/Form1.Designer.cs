﻿
namespace Argon
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label13;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.GroupBox groupBox2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label18;
            System.Windows.Forms.Label label19;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label17;
            System.Windows.Forms.Label label20;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label16;
            System.Windows.Forms.Label label21;
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.rdBtn_STW = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpBox_create = new System.Windows.Forms.GroupBox();
            this.checkBox_Vacancy = new System.Windows.Forms.CheckBox();
            this.textBox_procent = new System.Windows.Forms.TextBox();
            this.btn_openSecondNeigbords = new System.Windows.Forms.Button();
            this.btn_openFirstNeigborth = new System.Windows.Forms.Button();
            this.btn_openCoordAtoms = new System.Windows.Forms.Button();
            this.txt_latPar = new System.Windows.Forms.TextBox();
            this.txt_numAtoms = new System.Windows.Forms.TextBox();
            this.btn_createStructure = new System.Windows.Forms.Button();
            this.numUD_n = new System.Windows.Forms.NumericUpDown();
            this.grpBox_energy = new System.Windows.Forms.GroupBox();
            this.txt_atomEnergy = new System.Windows.Forms.TextBox();
            this.txt_sumEnergy = new System.Windows.Forms.TextBox();
            this.btn_calculateEnergy = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txt_tmprEnergy = new System.Windows.Forms.TextBox();
            this.numUD_multStepRelax = new System.Windows.Forms.NumericUpDown();
            this.numUD_stepNorm = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.checkBox_startVelocity = new System.Windows.Forms.CheckBox();
            this.numUD_step = new System.Windows.Forms.NumericUpDown();
            this.button_StartRelax = new System.Windows.Forms.Button();
            this.btn_clearStep = new System.Windows.Forms.Button();
            this.btn_deleteStep = new System.Windows.Forms.Button();
            this.btn_addStep = new System.Windows.Forms.Button();
            this.listBox_step = new System.Windows.Forms.ListBox();
            this.numUD_tmpr = new System.Windows.Forms.NumericUpDown();
            this.numUD_shiftAtoms = new System.Windows.Forms.NumericUpDown();
            this.numUD_numStepRelax = new System.Windows.Forms.NumericUpDown();
            label11 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            groupBox2 = new System.Windows.Forms.GroupBox();
            label1 = new System.Windows.Forms.Label();
            label18 = new System.Windows.Forms.Label();
            label19 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label17 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label16 = new System.Windows.Forms.Label();
            label21 = new System.Windows.Forms.Label();
            groupBox2.SuspendLayout();
            this.grpBox_create.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_n)).BeginInit();
            this.grpBox_energy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_multStepRelax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_stepNorm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_step)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_tmpr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_shiftAtoms)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_numStepRelax)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new System.Drawing.Point(222, 81);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(25, 17);
            label11.TabIndex = 15;
            label11.Text = "нм";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new System.Drawing.Point(6, 50);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(108, 17);
            label8.TabIndex = 22;
            label8.Text = "Кол-во атомов:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Cursor = System.Windows.Forms.Cursors.Help;
            label2.Location = new System.Drawing.Point(6, 25);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(187, 17);
            label2.TabIndex = 4;
            label2.Text = "Количество атомов в ряду:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(6, 83);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(139, 17);
            label4.TabIndex = 16;
            label4.Text = "Параметр решётки:";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(250, 109);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(24, 17);
            label14.TabIndex = 17;
            label14.Text = "эВ";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label13.Location = new System.Drawing.Point(9, 104);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(130, 17);
            label13.TabIndex = 16;
            label13.Text = "Энергия на атом ≈";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Location = new System.Drawing.Point(250, 78);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(24, 17);
            label10.TabIndex = 14;
            label10.Text = "эВ";
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(this.rdBtn_STW);
            groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            groupBox2.Location = new System.Drawing.Point(6, 19);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new System.Drawing.Size(268, 49);
            groupBox2.TabIndex = 1;
            groupBox2.TabStop = false;
            groupBox2.Text = "Тип потенциала";
            // 
            // rdBtn_STW
            // 
            this.rdBtn_STW.AutoSize = true;
            this.rdBtn_STW.Checked = true;
            this.rdBtn_STW.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdBtn_STW.Location = new System.Drawing.Point(6, 21);
            this.rdBtn_STW.Name = "rdBtn_STW";
            this.rdBtn_STW.Size = new System.Drawing.Size(150, 21);
            this.rdBtn_STW.TabIndex = 2;
            this.rdBtn_STW.TabStop = true;
            this.rdBtn_STW.Text = "Леннарда-Джонса";
            this.rdBtn_STW.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label1.Location = new System.Drawing.Point(9, 78);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(126, 17);
            label1.TabIndex = 2;
            label1.Text = "Энергия системы:";
            // 
            // label18
            // 
            label18.AutoSize = true;
            label18.Location = new System.Drawing.Point(476, 150);
            label18.Name = "label18";
            label18.Size = new System.Drawing.Size(24, 17);
            label18.TabIndex = 53;
            label18.Text = "эВ";
            label18.Visible = false;
            // 
            // label19
            // 
            label19.AutoSize = true;
            label19.Cursor = System.Windows.Forms.Cursors.Help;
            label19.Location = new System.Drawing.Point(263, 150);
            label19.Name = "label19";
            label19.Size = new System.Drawing.Size(104, 17);
            label19.TabIndex = 52;
            label19.Text = "Равн. энергия:";
            label19.Visible = false;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Cursor = System.Windows.Forms.Cursors.Help;
            label7.Location = new System.Drawing.Point(11, 78);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(122, 17);
            label7.TabIndex = 50;
            label7.Text = "Множитель шага:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new System.Drawing.Point(215, 169);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(17, 17);
            label5.TabIndex = 32;
            label5.Text = "К";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new System.Drawing.Point(11, 164);
            label17.Name = "label17";
            label17.Size = new System.Drawing.Size(100, 17);
            label17.TabIndex = 31;
            label17.Text = "Температура:";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Cursor = System.Windows.Forms.Cursors.Help;
            label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            label20.Location = new System.Drawing.Point(276, 21);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(120, 17);
            label20.TabIndex = 42;
            label20.Text = "Деление шага:";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(225, 122);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(25, 17);
            label12.TabIndex = 29;
            label12.Text = "нм";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new System.Drawing.Point(7, 122);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(138, 17);
            label9.TabIndex = 28;
            label9.Text = "Случ. сдвиг атомов:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(12, 34);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(133, 17);
            label6.TabIndex = 25;
            label6.Text = "Количество шагов:";
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new System.Drawing.Point(225, 142);
            label16.Name = "label16";
            label16.Size = new System.Drawing.Size(20, 17);
            label16.TabIndex = 31;
            label16.Text = "%";
            // 
            // label21
            // 
            label21.Location = new System.Drawing.Point(6, 142);
            label21.Name = "label21";
            label21.Size = new System.Drawing.Size(142, 58);
            label21.TabIndex = 29;
            label21.Text = "Процент удаляемых атомов";
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(15, 536);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(512, 89);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Моделирование вакансии";
            // 
            // grpBox_create
            // 
            this.grpBox_create.Controls.Add(this.checkBox_Vacancy);
            this.grpBox_create.Controls.Add(label16);
            this.grpBox_create.Controls.Add(this.textBox_procent);
            this.grpBox_create.Controls.Add(label21);
            this.grpBox_create.Controls.Add(this.btn_openSecondNeigbords);
            this.grpBox_create.Controls.Add(this.btn_openFirstNeigborth);
            this.grpBox_create.Controls.Add(this.btn_openCoordAtoms);
            this.grpBox_create.Controls.Add(label11);
            this.grpBox_create.Controls.Add(this.txt_latPar);
            this.grpBox_create.Controls.Add(this.txt_numAtoms);
            this.grpBox_create.Controls.Add(label8);
            this.grpBox_create.Controls.Add(this.btn_createStructure);
            this.grpBox_create.Controls.Add(this.numUD_n);
            this.grpBox_create.Controls.Add(label2);
            this.grpBox_create.Controls.Add(label4);
            this.grpBox_create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_create.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpBox_create.Location = new System.Drawing.Point(22, 12);
            this.grpBox_create.Name = "grpBox_create";
            this.grpBox_create.Size = new System.Drawing.Size(257, 225);
            this.grpBox_create.TabIndex = 3;
            this.grpBox_create.TabStop = false;
            this.grpBox_create.Text = "Параметры создания структуры";
            // 
            // checkBox_Vacancy
            // 
            this.checkBox_Vacancy.AutoSize = true;
            this.checkBox_Vacancy.Location = new System.Drawing.Point(10, 109);
            this.checkBox_Vacancy.Name = "checkBox_Vacancy";
            this.checkBox_Vacancy.Size = new System.Drawing.Size(201, 21);
            this.checkBox_Vacancy.TabIndex = 32;
            this.checkBox_Vacancy.Text = "Моделирование вакансии";
            this.checkBox_Vacancy.UseVisualStyleBackColor = true;
            // 
            // textBox_procent
            // 
            this.textBox_procent.BackColor = System.Drawing.SystemColors.Control;
            this.textBox_procent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_procent.ForeColor = System.Drawing.Color.DarkBlue;
            this.textBox_procent.Location = new System.Drawing.Point(163, 142);
            this.textBox_procent.Name = "textBox_procent";
            this.textBox_procent.Size = new System.Drawing.Size(61, 22);
            this.textBox_procent.TabIndex = 30;
            this.textBox_procent.TabStop = false;
            this.textBox_procent.Text = "20";
            // 
            // btn_openSecondNeigbords
            // 
            this.btn_openSecondNeigbords.BackColor = System.Drawing.Color.LightCyan;
            this.btn_openSecondNeigbords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openSecondNeigbords.Location = new System.Drawing.Point(303, 162);
            this.btn_openSecondNeigbords.Name = "btn_openSecondNeigbords";
            this.btn_openSecondNeigbords.Size = new System.Drawing.Size(202, 63);
            this.btn_openSecondNeigbords.TabIndex = 27;
            this.btn_openSecondNeigbords.TabStop = false;
            this.btn_openSecondNeigbords.Text = "Открыть координаты вторых соседей атомов в Excel";
            this.btn_openSecondNeigbords.UseVisualStyleBackColor = false;
            // 
            // btn_openFirstNeigborth
            // 
            this.btn_openFirstNeigborth.BackColor = System.Drawing.Color.LightCyan;
            this.btn_openFirstNeigborth.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openFirstNeigborth.Location = new System.Drawing.Point(303, 81);
            this.btn_openFirstNeigborth.Name = "btn_openFirstNeigborth";
            this.btn_openFirstNeigborth.Size = new System.Drawing.Size(202, 62);
            this.btn_openFirstNeigborth.TabIndex = 26;
            this.btn_openFirstNeigborth.TabStop = false;
            this.btn_openFirstNeigborth.Text = "Открыть координаты первых соседей атомов в Excel";
            this.btn_openFirstNeigborth.UseVisualStyleBackColor = false;
            // 
            // btn_openCoordAtoms
            // 
            this.btn_openCoordAtoms.BackColor = System.Drawing.Color.LightCyan;
            this.btn_openCoordAtoms.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_openCoordAtoms.Location = new System.Drawing.Point(303, 16);
            this.btn_openCoordAtoms.Name = "btn_openCoordAtoms";
            this.btn_openCoordAtoms.Size = new System.Drawing.Size(202, 51);
            this.btn_openCoordAtoms.TabIndex = 25;
            this.btn_openCoordAtoms.TabStop = false;
            this.btn_openCoordAtoms.Text = "Открыть  координаты атомов в Excel";
            this.btn_openCoordAtoms.UseVisualStyleBackColor = false;
            // 
            // txt_latPar
            // 
            this.txt_latPar.BackColor = System.Drawing.SystemColors.Control;
            this.txt_latPar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_latPar.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_latPar.Location = new System.Drawing.Point(151, 81);
            this.txt_latPar.Name = "txt_latPar";
            this.txt_latPar.ReadOnly = true;
            this.txt_latPar.Size = new System.Drawing.Size(60, 22);
            this.txt_latPar.TabIndex = 24;
            this.txt_latPar.TabStop = false;
            this.txt_latPar.Text = "0,54535";
            this.txt_latPar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_numAtoms
            // 
            this.txt_numAtoms.BackColor = System.Drawing.SystemColors.Control;
            this.txt_numAtoms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_numAtoms.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_numAtoms.Location = new System.Drawing.Point(119, 49);
            this.txt_numAtoms.Name = "txt_numAtoms";
            this.txt_numAtoms.ReadOnly = true;
            this.txt_numAtoms.Size = new System.Drawing.Size(57, 22);
            this.txt_numAtoms.TabIndex = 23;
            this.txt_numAtoms.TabStop = false;
            this.txt_numAtoms.Text = "64";
            this.txt_numAtoms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_numAtoms.TextChanged += new System.EventHandler(this.txt_numAtoms_TextChanged);
            // 
            // btn_createStructure
            // 
            this.btn_createStructure.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_createStructure.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_createStructure.Location = new System.Drawing.Point(167, 173);
            this.btn_createStructure.Name = "btn_createStructure";
            this.btn_createStructure.Size = new System.Drawing.Size(84, 40);
            this.btn_createStructure.TabIndex = 2;
            this.btn_createStructure.Text = "Создать";
            this.btn_createStructure.UseVisualStyleBackColor = false;
            this.btn_createStructure.Click += new System.EventHandler(this.btn_createStructure_Click);
            // 
            // numUD_n
            // 
            this.numUD_n.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_n.Location = new System.Drawing.Point(199, 23);
            this.numUD_n.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numUD_n.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numUD_n.Name = "numUD_n";
            this.numUD_n.Size = new System.Drawing.Size(42, 22);
            this.numUD_n.TabIndex = 0;
            this.numUD_n.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numUD_n.ValueChanged += new System.EventHandler(this.numUD_sizeCells_ValueChanged_1);
            // 
            // grpBox_energy
            // 
            this.grpBox_energy.Controls.Add(label14);
            this.grpBox_energy.Controls.Add(this.txt_atomEnergy);
            this.grpBox_energy.Controls.Add(label13);
            this.grpBox_energy.Controls.Add(label10);
            this.grpBox_energy.Controls.Add(groupBox2);
            this.grpBox_energy.Controls.Add(this.txt_sumEnergy);
            this.grpBox_energy.Controls.Add(label1);
            this.grpBox_energy.Controls.Add(this.btn_calculateEnergy);
            this.grpBox_energy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpBox_energy.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grpBox_energy.Location = new System.Drawing.Point(285, 12);
            this.grpBox_energy.Name = "grpBox_energy";
            this.grpBox_energy.Size = new System.Drawing.Size(282, 225);
            this.grpBox_energy.TabIndex = 4;
            this.grpBox_energy.TabStop = false;
            this.grpBox_energy.Text = "Параметры рассчёта энергии";
            // 
            // txt_atomEnergy
            // 
            this.txt_atomEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_atomEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_atomEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_atomEnergy.Location = new System.Drawing.Point(144, 104);
            this.txt_atomEnergy.Name = "txt_atomEnergy";
            this.txt_atomEnergy.ReadOnly = true;
            this.txt_atomEnergy.Size = new System.Drawing.Size(100, 22);
            this.txt_atomEnergy.TabIndex = 15;
            this.txt_atomEnergy.TabStop = false;
            this.txt_atomEnergy.Text = "0";
            this.txt_atomEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_sumEnergy
            // 
            this.txt_sumEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_sumEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_sumEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_sumEnergy.Location = new System.Drawing.Point(144, 78);
            this.txt_sumEnergy.Name = "txt_sumEnergy";
            this.txt_sumEnergy.ReadOnly = true;
            this.txt_sumEnergy.Size = new System.Drawing.Size(100, 22);
            this.txt_sumEnergy.TabIndex = 1;
            this.txt_sumEnergy.TabStop = false;
            this.txt_sumEnergy.Text = "0";
            this.txt_sumEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_calculateEnergy
            // 
            this.btn_calculateEnergy.BackColor = System.Drawing.Color.PaleTurquoise;
            this.btn_calculateEnergy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_calculateEnergy.Location = new System.Drawing.Point(12, 142);
            this.btn_calculateEnergy.Name = "btn_calculateEnergy";
            this.btn_calculateEnergy.Size = new System.Drawing.Size(123, 46);
            this.btn_calculateEnergy.TabIndex = 0;
            this.btn_calculateEnergy.Text = "Подсчёт энергии системы";
            this.btn_calculateEnergy.UseVisualStyleBackColor = false;
            this.btn_calculateEnergy.Click += new System.EventHandler(this.btn_calculateEnergy_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(590, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(620, 316);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Location = new System.Drawing.Point(1250, 12);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(403, 334);
            this.chart1.TabIndex = 6;
            this.chart1.Text = "chart1";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(label18);
            this.groupBox3.Controls.Add(this.txt_tmprEnergy);
            this.groupBox3.Controls.Add(label19);
            this.groupBox3.Controls.Add(this.numUD_multStepRelax);
            this.groupBox3.Controls.Add(label7);
            this.groupBox3.Controls.Add(this.numUD_stepNorm);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.checkBox_startVelocity);
            this.groupBox3.Controls.Add(this.numUD_step);
            this.groupBox3.Controls.Add(this.button_StartRelax);
            this.groupBox3.Controls.Add(this.btn_clearStep);
            this.groupBox3.Controls.Add(label5);
            this.groupBox3.Controls.Add(this.btn_deleteStep);
            this.groupBox3.Controls.Add(this.btn_addStep);
            this.groupBox3.Controls.Add(label17);
            this.groupBox3.Controls.Add(this.listBox_step);
            this.groupBox3.Controls.Add(this.numUD_tmpr);
            this.groupBox3.Controls.Add(label20);
            this.groupBox3.Controls.Add(label12);
            this.groupBox3.Controls.Add(label9);
            this.groupBox3.Controls.Add(this.numUD_shiftAtoms);
            this.groupBox3.Controls.Add(this.numUD_numStepRelax);
            this.groupBox3.Controls.Add(label6);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(22, 243);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(545, 287);
            this.groupBox3.TabIndex = 40;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Релаксация структуры";
            // 
            // txt_tmprEnergy
            // 
            this.txt_tmprEnergy.BackColor = System.Drawing.SystemColors.Control;
            this.txt_tmprEnergy.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_tmprEnergy.ForeColor = System.Drawing.Color.DarkBlue;
            this.txt_tmprEnergy.Location = new System.Drawing.Point(385, 148);
            this.txt_tmprEnergy.Name = "txt_tmprEnergy";
            this.txt_tmprEnergy.ReadOnly = true;
            this.txt_tmprEnergy.Size = new System.Drawing.Size(85, 22);
            this.txt_tmprEnergy.TabIndex = 51;
            this.txt_tmprEnergy.TabStop = false;
            this.txt_tmprEnergy.Text = "0";
            this.txt_tmprEnergy.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_tmprEnergy.Visible = false;
            // 
            // numUD_multStepRelax
            // 
            this.numUD_multStepRelax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_multStepRelax.DecimalPlaces = 3;
            this.numUD_multStepRelax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_multStepRelax.Location = new System.Drawing.Point(150, 78);
            this.numUD_multStepRelax.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_multStepRelax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_multStepRelax.Name = "numUD_multStepRelax";
            this.numUD_multStepRelax.Size = new System.Drawing.Size(59, 22);
            this.numUD_multStepRelax.TabIndex = 49;
            this.numUD_multStepRelax.Value = new decimal(new int[] {
            10,
            0,
            0,
            196608});
            // 
            // numUD_stepNorm
            // 
            this.numUD_stepNorm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_stepNorm.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_stepNorm.Location = new System.Drawing.Point(256, 205);
            this.numUD_stepNorm.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_stepNorm.Name = "numUD_stepNorm";
            this.numUD_stepNorm.Size = new System.Drawing.Size(44, 22);
            this.numUD_stepNorm.TabIndex = 47;
            this.numUD_stepNorm.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Cursor = System.Windows.Forms.Cursors.Help;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(7, 204);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(217, 17);
            this.label23.TabIndex = 48;
            this.label23.Text = "Период нормировки скоростей:";
            // 
            // checkBox_startVelocity
            // 
            this.checkBox_startVelocity.AutoSize = true;
            this.checkBox_startVelocity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox_startVelocity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.checkBox_startVelocity.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_startVelocity.Location = new System.Drawing.Point(6, 233);
            this.checkBox_startVelocity.Name = "checkBox_startVelocity";
            this.checkBox_startVelocity.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBox_startVelocity.Size = new System.Drawing.Size(241, 38);
            this.checkBox_startVelocity.TabIndex = 34;
            this.checkBox_startVelocity.Text = "Начальные скорости\r\nпропорциональны температуре:";
            this.checkBox_startVelocity.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox_startVelocity.UseVisualStyleBackColor = true;
            // 
            // numUD_step
            // 
            this.numUD_step.BackColor = System.Drawing.SystemColors.HighlightText;
            this.numUD_step.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_step.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_step.Location = new System.Drawing.Point(281, 50);
            this.numUD_step.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_step.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_step.Name = "numUD_step";
            this.numUD_step.Size = new System.Drawing.Size(80, 22);
            this.numUD_step.TabIndex = 46;
            this.numUD_step.TabStop = false;
            this.numUD_step.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // button_StartRelax
            // 
            this.button_StartRelax.BackColor = System.Drawing.Color.LightCyan;
            this.button_StartRelax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_StartRelax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_StartRelax.Location = new System.Drawing.Point(374, 224);
            this.button_StartRelax.Name = "button_StartRelax";
            this.button_StartRelax.Size = new System.Drawing.Size(96, 47);
            this.button_StartRelax.TabIndex = 33;
            this.button_StartRelax.Text = "Запуск";
            this.button_StartRelax.UseVisualStyleBackColor = false;
            this.button_StartRelax.Click += new System.EventHandler(this.button_StartRelax_Click);
            // 
            // btn_clearStep
            // 
            this.btn_clearStep.BackColor = System.Drawing.Color.LightCyan;
            this.btn_clearStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_clearStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_clearStep.Location = new System.Drawing.Point(367, 109);
            this.btn_clearStep.Name = "btn_clearStep";
            this.btn_clearStep.Size = new System.Drawing.Size(85, 30);
            this.btn_clearStep.TabIndex = 45;
            this.btn_clearStep.TabStop = false;
            this.btn_clearStep.Text = "Очистить";
            this.btn_clearStep.UseVisualStyleBackColor = false;
            // 
            // btn_deleteStep
            // 
            this.btn_deleteStep.BackColor = System.Drawing.Color.LightCyan;
            this.btn_deleteStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_deleteStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_deleteStep.Location = new System.Drawing.Point(367, 78);
            this.btn_deleteStep.Name = "btn_deleteStep";
            this.btn_deleteStep.Size = new System.Drawing.Size(85, 30);
            this.btn_deleteStep.TabIndex = 44;
            this.btn_deleteStep.TabStop = false;
            this.btn_deleteStep.Text = "Удалить";
            this.btn_deleteStep.UseVisualStyleBackColor = false;
            // 
            // btn_addStep
            // 
            this.btn_addStep.BackColor = System.Drawing.Color.LightCyan;
            this.btn_addStep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_addStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btn_addStep.Location = new System.Drawing.Point(367, 47);
            this.btn_addStep.Name = "btn_addStep";
            this.btn_addStep.Size = new System.Drawing.Size(85, 29);
            this.btn_addStep.TabIndex = 43;
            this.btn_addStep.TabStop = false;
            this.btn_addStep.Text = "Добавить";
            this.btn_addStep.UseVisualStyleBackColor = false;
            // 
            // listBox_step
            // 
            this.listBox_step.FormattingEnabled = true;
            this.listBox_step.ItemHeight = 16;
            this.listBox_step.Location = new System.Drawing.Point(281, 88);
            this.listBox_step.Name = "listBox_step";
            this.listBox_step.Size = new System.Drawing.Size(80, 52);
            this.listBox_step.TabIndex = 41;
            this.listBox_step.TabStop = false;
            // 
            // numUD_tmpr
            // 
            this.numUD_tmpr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_tmpr.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numUD_tmpr.Location = new System.Drawing.Point(155, 164);
            this.numUD_tmpr.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUD_tmpr.Name = "numUD_tmpr";
            this.numUD_tmpr.Size = new System.Drawing.Size(54, 22);
            this.numUD_tmpr.TabIndex = 30;
            this.numUD_tmpr.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // numUD_shiftAtoms
            // 
            this.numUD_shiftAtoms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_shiftAtoms.DecimalPlaces = 3;
            this.numUD_shiftAtoms.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.numUD_shiftAtoms.Location = new System.Drawing.Point(160, 122);
            this.numUD_shiftAtoms.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUD_shiftAtoms.Name = "numUD_shiftAtoms";
            this.numUD_shiftAtoms.Size = new System.Drawing.Size(59, 22);
            this.numUD_shiftAtoms.TabIndex = 27;
            this.numUD_shiftAtoms.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // numUD_numStepRelax
            // 
            this.numUD_numStepRelax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.numUD_numStepRelax.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numUD_numStepRelax.Location = new System.Drawing.Point(150, 32);
            this.numUD_numStepRelax.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numUD_numStepRelax.Minimum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numUD_numStepRelax.Name = "numUD_numStepRelax";
            this.numUD_numStepRelax.Size = new System.Drawing.Size(59, 22);
            this.numUD_numStepRelax.TabIndex = 24;
            this.numUD_numStepRelax.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1527, 723);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.grpBox_energy);
            this.Controls.Add(this.grpBox_create);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            this.grpBox_create.ResumeLayout(false);
            this.grpBox_create.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_n)).EndInit();
            this.grpBox_energy.ResumeLayout(false);
            this.grpBox_energy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_multStepRelax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_stepNorm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_step)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_tmpr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_shiftAtoms)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUD_numStepRelax)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox grpBox_create;
        private System.Windows.Forms.Button btn_openSecondNeigbords;
        private System.Windows.Forms.Button btn_openFirstNeigborth;
        private System.Windows.Forms.Button btn_openCoordAtoms;
        private System.Windows.Forms.TextBox txt_latPar;
        private System.Windows.Forms.TextBox txt_numAtoms;
        private System.Windows.Forms.Button btn_createStructure;
        private System.Windows.Forms.NumericUpDown numUD_n;
        private System.Windows.Forms.GroupBox grpBox_energy;
        private System.Windows.Forms.TextBox txt_atomEnergy;
        private System.Windows.Forms.RadioButton rdBtn_STW;
        private System.Windows.Forms.TextBox txt_sumEnergy;
        private System.Windows.Forms.Button btn_calculateEnergy;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txt_tmprEnergy;
        private System.Windows.Forms.NumericUpDown numUD_multStepRelax;
        private System.Windows.Forms.NumericUpDown numUD_stepNorm;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.CheckBox checkBox_startVelocity;
        private System.Windows.Forms.NumericUpDown numUD_step;
        private System.Windows.Forms.Button button_StartRelax;
        private System.Windows.Forms.Button btn_clearStep;
        private System.Windows.Forms.Button btn_deleteStep;
        private System.Windows.Forms.Button btn_addStep;
        private System.Windows.Forms.ListBox listBox_step;
        private System.Windows.Forms.NumericUpDown numUD_tmpr;
        private System.Windows.Forms.NumericUpDown numUD_shiftAtoms;
        private System.Windows.Forms.NumericUpDown numUD_numStepRelax;
        private System.Windows.Forms.TextBox textBox_procent;
        private System.Windows.Forms.CheckBox checkBox_Vacancy;
    }
}


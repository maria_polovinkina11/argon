﻿
namespace Argon
{
    partial class FormConsole
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.GroupBox groupBox2;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            this.pgsBar_time = new System.Windows.Forms.ProgressBar();
            this.txtBox_time = new System.Windows.Forms.TextBox();
            this.label_progress = new System.Windows.Forms.Label();
            this.txtBox_lastTime = new System.Windows.Forms.TextBox();
            this.txtBox_output = new System.Windows.Forms.TextBox();
            this.check_outputPause = new System.Windows.Forms.CheckBox();
            this.btn_break = new System.Windows.Forms.Button();
            this.timer_update = new System.Windows.Forms.Timer(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            groupBox2 = new System.Windows.Forms.GroupBox();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(this.pgsBar_time);
            groupBox2.Controls.Add(this.txtBox_time);
            groupBox2.Controls.Add(this.label_progress);
            groupBox2.Controls.Add(this.txtBox_lastTime);
            groupBox2.Controls.Add(label2);
            groupBox2.Location = new System.Drawing.Point(495, 13);
            groupBox2.Margin = new System.Windows.Forms.Padding(4);
            groupBox2.Name = "groupBox2";
            groupBox2.Padding = new System.Windows.Forms.Padding(4);
            groupBox2.Size = new System.Drawing.Size(269, 194);
            groupBox2.TabIndex = 18;
            groupBox2.TabStop = false;
            groupBox2.Text = "Прогресс";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 32);
            label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(139, 17);
            label1.TabIndex = 3;
            label1.Text = "Время выполнения:";
            // 
            // pgsBar_time
            // 
            this.pgsBar_time.Location = new System.Drawing.Point(20, 74);
            this.pgsBar_time.Margin = new System.Windows.Forms.Padding(4);
            this.pgsBar_time.Name = "pgsBar_time";
            this.pgsBar_time.Size = new System.Drawing.Size(177, 28);
            this.pgsBar_time.TabIndex = 2;
            // 
            // txtBox_time
            // 
            this.txtBox_time.BackColor = System.Drawing.SystemColors.Control;
            this.txtBox_time.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_time.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBox_time.ForeColor = System.Drawing.Color.Navy;
            this.txtBox_time.Location = new System.Drawing.Point(173, 30);
            this.txtBox_time.Margin = new System.Windows.Forms.Padding(4);
            this.txtBox_time.Name = "txtBox_time";
            this.txtBox_time.ReadOnly = true;
            this.txtBox_time.Size = new System.Drawing.Size(79, 22);
            this.txtBox_time.TabIndex = 5;
            this.txtBox_time.TabStop = false;
            this.txtBox_time.Text = "00h 00m";
            this.txtBox_time.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_progress
            // 
            this.label_progress.AutoSize = true;
            this.label_progress.Location = new System.Drawing.Point(209, 79);
            this.label_progress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_progress.Name = "label_progress";
            this.label_progress.Size = new System.Drawing.Size(28, 17);
            this.label_progress.TabIndex = 8;
            this.label_progress.Text = "0%";
            this.label_progress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtBox_lastTime
            // 
            this.txtBox_lastTime.BackColor = System.Drawing.SystemColors.Control;
            this.txtBox_lastTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_lastTime.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBox_lastTime.ForeColor = System.Drawing.Color.Navy;
            this.txtBox_lastTime.Location = new System.Drawing.Point(173, 148);
            this.txtBox_lastTime.Margin = new System.Windows.Forms.Padding(4);
            this.txtBox_lastTime.Name = "txtBox_lastTime";
            this.txtBox_lastTime.ReadOnly = true;
            this.txtBox_lastTime.Size = new System.Drawing.Size(79, 22);
            this.txtBox_lastTime.TabIndex = 6;
            this.txtBox_lastTime.TabStop = false;
            this.txtBox_lastTime.Text = "00h 00m";
            this.txtBox_lastTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(16, 150);
            label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(143, 17);
            label2.TabIndex = 7;
            label2.Text = "Примерно осталось:";
            // 
            // txtBox_output
            // 
            this.txtBox_output.BackColor = System.Drawing.Color.White;
            this.txtBox_output.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBox_output.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtBox_output.Location = new System.Drawing.Point(13, 13);
            this.txtBox_output.Margin = new System.Windows.Forms.Padding(4);
            this.txtBox_output.MaxLength = 0;
            this.txtBox_output.Multiline = true;
            this.txtBox_output.Name = "txtBox_output";
            this.txtBox_output.ReadOnly = true;
            this.txtBox_output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBox_output.Size = new System.Drawing.Size(474, 391);
            this.txtBox_output.TabIndex = 14;
            // 
            // check_outputPause
            // 
            this.check_outputPause.AutoSize = true;
            this.check_outputPause.Cursor = System.Windows.Forms.Cursors.Help;
            this.check_outputPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.check_outputPause.Location = new System.Drawing.Point(495, 215);
            this.check_outputPause.Margin = new System.Windows.Forms.Padding(4);
            this.check_outputPause.Name = "check_outputPause";
            this.check_outputPause.Size = new System.Drawing.Size(171, 21);
            this.check_outputPause.TabIndex = 19;
            this.check_outputPause.Text = "Приостановить вывод";
            this.check_outputPause.UseVisualStyleBackColor = true;
            // 
            // btn_break
            // 
            this.btn_break.BackColor = System.Drawing.Color.Red;
            this.btn_break.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_break.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_break.Location = new System.Drawing.Point(664, 361);
            this.btn_break.Margin = new System.Windows.Forms.Padding(4);
            this.btn_break.Name = "btn_break";
            this.btn_break.Size = new System.Drawing.Size(100, 28);
            this.btn_break.TabIndex = 20;
            this.btn_break.Text = "Прервать";
            this.btn_break.UseVisualStyleBackColor = false;
            this.btn_break.Visible = false;
            this.btn_break.Click += new System.EventHandler(this.btn_break_Click);
            // 
            // timer_update
            // 
            this.timer_update.Tick += new System.EventHandler(this.timer_update_Tick);
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 8000;
            this.toolTip.InitialDelay = 500;
            this.toolTip.ReshowDelay = 100;
            // 
            // FormConsole
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 416);
            this.Controls.Add(this.btn_break);
            this.Controls.Add(this.check_outputPause);
            this.Controls.Add(groupBox2);
            this.Controls.Add(this.txtBox_output);
            this.Name = "FormConsole";
            this.Text = "FormConsole";
            this.Load += new System.EventHandler(this.FormConsole_Load);
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBox_output;
        private System.Windows.Forms.CheckBox check_outputPause;
        private System.Windows.Forms.ProgressBar pgsBar_time;
        private System.Windows.Forms.TextBox txtBox_time;
        private System.Windows.Forms.Label label_progress;
        private System.Windows.Forms.TextBox txtBox_lastTime;
        private System.Windows.Forms.Button btn_break;
        private System.Windows.Forms.Timer timer_update;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace Argon
{
    public static class OutputInfo
    {
        private static string dirData = Environment.CurrentDirectory + "\\Data\\";
        private static string pathCordAtoms = dirData + "PositionsAtoms.csv";
        private static string pathFirstNeighboursdAtoms = dirData + "FirstNeighbours.csv";
        private static string pathSecondNeighboursdAtoms = dirData + "SecondNeighbours.csv";
        private static string pathEnergy = dirData + "Energy.csv";
        /// <summary>
        /// Проверяет наличие каталогов
        /// </summary>
        public static void Start()
        {
            DirectoryInfo diData = new DirectoryInfo(dirData);


            if (diData.Exists)
            {

                return;

            }
            else
            {
                diData.Create();

            }
        }

        /// <summary>
        /// Записать энергию в файл
        /// </summary>
        /// <param name="step">Номер шага по времени</param>
        /// <param name="timeStep">Шаг по времени</param>
        /// <param name="kinEnergy">Кинетическая энергия на данном шаге</param>
        /// <param name="potEnergy">Потенциальная энергия на данном шаге</param>
        /// <param name="startPotEnergy">Начальная потенциальная энергия</param>
        /// <param name="test">Это файл из ряда тестов? Если true, то он сохраняется по шаблону в папке с тестами</param>
        /// <returns></returns>
        /* public static bool WriteEnergyFile(int step, double EnergyDuo, double EnergyTrio, double sumEnergy)
         {
             string path;
             path = pathEnergy;

             using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
             {
                 sw.WriteLine("{0};;{1};{2};{3};", step, EnergyDuo, EnergyTrio, sumEnergy);
             }
             return true;
         }*/
        /// <summary>
        /// Записать энергию в файл
        /// </summary>
        /// <param name="step">Номер шага по времени</param>
        /// <param name="timeStep">Шаг по времени</param>
        /// <param name="kinEnergy">Кинетическая энергия на данном шаге</param>
        /// <param name="potEnergy">Потенциальная энергия на данном шаге</param>
        /// <param name="startPotEnergy">Начальная потенциальная энергия</param>
        /// <param name="test">Это файл из ряда тестов? Если true, то он сохраняется по шаблону в папке с тестами</param>
        /// <returns></returns>
        public static bool WriteEnergyFile(int step, double timeStep, double kinEnergy, double potEnergy, double startPotEnergy)
        {
            string path = pathEnergy;

            using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
            {
                sw.WriteLine("{0};{1};;{2};{3};{4}", step, timeStep, potEnergy, kinEnergy, kinEnergy + potEnergy - startPotEnergy);
            }
            return true;
        }


        /// <summary>
        /// Записать координаты атомов в файл
        /// </summary>
        /// <param name="index">Индекс атома</param>
        /// <param name="x">Координата атома по Х</param>
        /// <param name="y">Координата атома по Y</param>
        /// <param name="z">Координата атома по Z</param>

        /// <returns></returns>
        public static bool WriteCoordAtomFile(int index, double x, double y)
        {
            string path;
            path = pathCordAtoms;

            using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
            {
                sw.WriteLine("{0};{1};{2};", index, x, y);
            }
            return true;
        }

        /// <summary>
        /// Записать координаты первых соседей атомов в файл
        /// </summary>
        /// <param name="index">Индекс атома</param>
        /// <param name="x">Координата атома по Х</param>
        /// <param name="y">Координата атома по Y</param>
        /// <param name="z">Координата атома по Z</param>

        /// <returns></returns>
        public static bool WriteFirstNeighboursdAtoms(int index_atoms, int index_atom1, double x1, double y1, double z1,
                                                                       int index_atom2, double x2, double y2, double z2,
                                                                       int index_atom3, double x3, double y3, double z3,
                                                                       int index_atom4, double x4, double y4, double z4)
        {
            string path;
            path = pathFirstNeighboursdAtoms;

            using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
            {
                sw.WriteLine(
                    "{0};;{1};{2};{3};{4};;{5};{6};{7};{8};;{9};{10};{11};{12};;{13};{14};{15};{15};",
                    index_atoms,
                    index_atom1, x1, y1, z1,
                    index_atom2, x2, y2, z2,
                    index_atom3, x3, y3, z3,
                    index_atom4, x4, y4, z4);
            }
            return true;

        }

        /// <summary>
        /// Записать координаты первых соседей атомов в файл
        /// </summary>
        /// <param name="index">Индекс атома</param>
        /// <param name="x">Координата атома по Х</param>
        /// <param name="y">Координата атома по Y</param>
        /// <param name="z">Координата атома по Z</param>

        /// <returns></returns>
        public static bool WriteSecondNeighboursdAtoms(int index_atoms, int index_atoms1, double x1, double y1,
                                                                        int index_atoms2, double x2, double y2, 
                                                                        int index_atoms3, double x3, double y3,                                                                         int index_atoms4, double x4, double y4, double z4,
                                                                        int index_atoms5, double x5, double y5, 
                                                                        int index_atoms6, double x6, double y6, 
                                                                        int index_atoms7, double x7, double y7, 
                                                                        int index_atoms8, double x8, double y8, 
                                                                        int index_atoms9, double x9, double y9, 
                                                                        int index_atoms10, double x10, double y10,
                                                                        int index_atoms11, double x11, double y11,
                                                                        int index_atoms12, double x12, double y12)

        {
            string path;
            path = pathSecondNeighboursdAtoms;

            using (StreamWriter sw = new StreamWriter(path, true, Encoding.Default))
            {
                sw.WriteLine(
                    "{0};;{1};{2};{3};;" +
                         "{4};{5};{5};;" +
                         "{6};{7};{8};;" +
                         "{8};{9};{10};;" +
                         "{11};{12};{13};;" +
                         "{14};{15};{16};;" +
                         "{17};{18};{19};;" +
                         "{20};{21};{22};;" +
                         "{23};{24};{25};;" +
                         "{26};{27};{28};;" +
                         "{29};{30};{31};;" +
                         "{32};{33};{34}",
                    index_atoms,
                    index_atoms1, x1, y1,
                    index_atoms2, x2, y2,
                    index_atoms3, x3, y3,
                    index_atoms4, x4, y4,
                    index_atoms5, x5, y5,
                    index_atoms6, x6, y6,
                    index_atoms7, x7, y7,
                    index_atoms8, x8, y8,
                    index_atoms9, x9, y9,
                    index_atoms10, x10, y10,
                    index_atoms11, x11, y11,
                    index_atoms12, x12, y12);
            }
            return true;

        }


        /// <summary>
        /// Очищает и создает новый файл
        /// </summary>
        /// <param name="tempr">Температура. Необходимо для файла из ряда тестов</param>
        /// <returns></returns>
        public static bool ClearEnergyFile(double tempr = 0)
        {
            try
            {
                string path;
                path = pathEnergy;

                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("Номер шага;Шаг по времени; ; Ep, эВ;  Ek, эВ; Esum, эВ;");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        /// <summary>
        /// Очищает и создает новый файл
        /// </summary>
        /// <param name="tempr">Температура. Необходимо для файла из ряда тестов</param>
        /// <returns></returns>
        public static bool ClearCoordAtomsFile(double tempr = 0)
        {
            try
            {
                string path;
                path = pathCordAtoms;

                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("Индекс атома;Координата X;Координата Y; Координата Z");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }
        /// <summary>
        /// Очищает и создает новый файл
        /// </summary>
        /// <param name="tempr">Температура. Необходимо для файла из ряда тестов</param>
        /// <returns></returns>
        public static bool ClearFirstNeighboursdAtoms(double tempr = 0)
        {
            try
            {
                string path;
                path = pathFirstNeighboursdAtoms;

                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("Индекс атома;; Сосед №1(индекс); Координата X; Координата Y; Координата Z;;" +
                                                "Сосед №2(индекс); Координата X; Координата Y; Координата Z;;" +
                                                "Сосед №3(индекс); Координата X; Координата Y; Координата Z;;" +
                                                "Сосед №4(индекс); Координата X; Координата Y; Координата Z");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        /// <summary>
        /// Очищает и создает новый файл
        /// </summary>
        /// <param name="tempr">Температура. Необходимо для файла из ряда тестов</param>
        /// <returns></returns>
        public static bool ClearSecondNeighboursdAtoms(double tempr = 0)
        {
            try
            {
                string path;
                path = pathSecondNeighboursdAtoms;

                using (StreamWriter sw = new StreamWriter(path, false, Encoding.Default))
                {
                    sw.WriteLine("Индекс атома;;Сосед №1(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №2(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №3(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №4(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №5(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №6(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №7(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №8(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №9(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №10(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №11(индекс); Координата X; Координата Y; Координата Z;;" +
                                               "Сосед №12(индекс); Координата X; Координата Y; Координата Z");
                }
                return true;
            }
            catch (System.IO.IOException)
            {
                return false;
            }
        }

        /// <summary>
        /// Открывает папку, где лежат сохранённые файлы
        /// </summary>
        public static void OpenStorage()
        {
            Process.Start("explorer", Environment.CurrentDirectory + "\\Data\\");
        }
        /// <summary>
        /// Открывает файл с 'энергией
        /// </summary>
        /// <returns></returns>
        public static bool OpenEnergyFile()
        {
            try
            {
                Process.Start(pathEnergy);
                return true;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return false;
            }

        }

        /// <summary>
        /// Открывает файл с координатами атомов
        /// </summary>
        /// <returns></returns>
        public static bool OpenCoordAtomsFile()
        {
            try
            {
                Process.Start(pathCordAtoms);
                return true;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return false;
            }

        }


        /// <summary>
        /// Открывает файл с координатами первых соседей атомов в структуре
        /// </summary>
        /// <returns></returns>
        public static bool OpenFirstNeighboursdAtomsFile()
        {
            try
            {
                Process.Start(pathFirstNeighboursdAtoms);
                return true;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Открывает файл с координатами вторых соседей атомов в структуре
        /// </summary>
        /// <returns></returns>
        public static bool OpenSecondNeighboursdAtomsFile()
        {
            try
            {
                Process.Start(pathSecondNeighboursdAtoms);
                return true;
            }
            catch (System.ComponentModel.Win32Exception)
            {
                return false;
            }
        }

    }
    }
